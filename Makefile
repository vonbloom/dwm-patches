fetch: curl
	@i=0; while read url; do \
		curl -s -o "$$i"0-"$$(echo $$url | awk -F'/' '{print $$6}')" "$$url"; let "i++"; \
	done < patches.orig

curl:
	@sudo pacman --needed -S $@

clean:
	@ls | grep '^[0-9]0' && ls | grep '^[0-9]0' | xargs rm || > /dev/null

.PHONY: curl fetch clean
